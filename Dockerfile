FROM node:slim AS builder

COPY . .

RUN npm install &&\
    npm run lint &&\
    npm run build

FROM nginx:alpine AS runtime

RUN apk update && apk add\
    nodejs\
    npm\ 
    bash

COPY --from=builder package.json /koa-server/
#COPY --from=builder .env /koa-server/
COPY --from=builder .env.prd /koa-server/
COPY --from=builder /dist /koa-server/dist/
RUN cd koa-server && npm install
COPY /nginx/nginx.conf /etc/nginx/nginx.conf
#COPY --from=builder /frontend/build /usr/share/nginx/html/

ADD /nginx/start.sh /
RUN chmod +x /start.sh

CMD /start.sh
