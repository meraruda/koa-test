echo "-- starting nginx ---"
sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/nginx.conf && nginx
echo "-- starting api server ---"
cd koa-server && npm run start:prd
