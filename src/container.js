import { createContainer, Lifetime } from 'awilix';

const container = createContainer();

container.loadModules(
    [`${process.env.CODEBASE}/service/*.js`, Lifetime.SCOPED],
    {
        formatName: 'camelCase',
    }
);

export default container;
