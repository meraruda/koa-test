import { route, GET, POST, before } from 'awilix-koa';
import { request, summary, path, body } from 'koa-swagger-decorator';
import SampleSchema from '~/model/sample';
import bodyParser from 'koa-bodyparser'

@route(`${process.env.API_PREFIX}/sample`)
export default class SampleController {
    constructor(props) {
        const { sampleService } = props;
        this.SampleService = sampleService;
    }

    @GET()
    @request('get', '/sample')
    @summary('get sample list')
    async getAll(ctx) {
        ctx.body = 'hello sample';
        ctx.status = 200;
    }

    @route('/:id')
    @GET()
    @request('get', '/sample/{id}')
    @summary('get sample by id')
    @path({
        id: { type: 'number', required: true, default: 1, description: 'id' },
    })
    async get(ctx) {
        ctx.body = this.SampleService.getSampleById(ctx.params.id);
        ctx.status = 200;
    }

    @POST()
    @before([bodyParser()])
    @request('POST', '/sample')
    @summary('register sample')
    @body(SampleSchema)
    async create(ctx) {
        // const { Email, NickName } = ctx.validateParams;
        // console.log(Email, NickName);
        // console.log(ctx.validateParams);
        console.log(ctx);
        ctx.status = 200;
    }
}