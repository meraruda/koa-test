import Koa from 'koa';
import morgan from 'koa-morgan';
import { loadControllers, scopePerRequest } from 'awilix-koa';
import container from './container';
import swagger from './swagger';
import dotenv from 'dotenv';

dotenv.config();

const app = new Koa();

app.use(morgan('combined'));

app.use(scopePerRequest(container));
app.use(loadControllers(`${process.env.CODEBASE}/controller/*.js`), { cwd: __dirname });

app.use(swagger.routes());

app.listen(process.env.PORT, () => console.log(`is listening port ${process.env.PORT}`));

export default app;