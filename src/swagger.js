import { SwaggerRouter } from 'koa-swagger-decorator';

const koaRouterOpts = { prefix: process.env.API_PREFIX };

const router = new SwaggerRouter(koaRouterOpts);

// swagger docs avaliable at http://localhost:[port]/api/swagger-html
router.swagger({
    title: 'Example Server',
    description: 'API DOC',
    version: '1.0.0',
   
    // [optional] default is root path.
    // if you are using koa-swagger-decorator within nested router, using this param to let swagger know your current router point
    prefix: process.env.API_PREFIX,
   
    // [optional] default is /swagger-html
    swaggerHtmlEndpoint: '/swagger-html',
   
    // [optional] default is /swagger-json
    swaggerJsonEndpoint: '/swagger-json',
   
    // [optional] additional options for building swagger doc
    // eg. add api_key as shown below
    swaggerOptions: {
      securityDefinitions: {
        api_key: {
          type: 'apiKey',
          in: 'header',
          name: 'api_key',
        },
      },
    },
    // [optional] additional configuration for config how to show swagger view
    swaggerConfiguration: {
      display: {
        defaultModelsExpandDepth: 4, // The default expansion depth for models (set to -1 completely hide the models).
        defaultModelExpandDepth: 3, // The default expansion depth for the model on the model-example section.
        docExpansion: 'list', // Controls the default expansion setting for the operations and tags. 
        defaultModelRendering: 'model' // Controls how the model is shown when the API is first rendered. 
      }
    }
  });
router.mapDir(__dirname);

export default router;